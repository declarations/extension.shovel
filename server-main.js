// A standard express wrapper server!
// What do you mean standard? What is a standard? This is not standard at all it is exceptional (like u)

var express = require("express");
var cors = require("cors");
var path = require("path");

let shovel = require("/home/debian/shovel/extension.shovel/shovel-server.js"); // Shovel System

var app = express();
app.use(cors());

app.get("/", function (req, res) {
    shovel.go(req, res);
});

var server = app.listen(3010, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Web Server listening at http://%s:%s", host, port);
});

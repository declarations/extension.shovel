// This script supporters server functionality! But also works if the server is offline!
// Would you like to play a game? Guess a number between 1 and 10, was it...... 7? If it was not you loose! You should have guessed 7!

const server_url = "https://shovel.declarations.style"; // Update with the address of da server!
//const server_url = "http://localhost:3000/shovel"; // Update with the address of da server!

const shovel_style = `#cvfc p::after {content:":root{@}"}
#cvfc:hover p::after {content:":root{"}
#cvfc:hover table::after {content:"}"}
#cvfc {
    z-index:10000;
    position:fixed;
    top:10px;
    left:10px;
    background:whitesmoke;
    border:1px solid transparent;
    font-family:monospace;
    overflow:hidden;
    border-radius:5px;
    padding:10px;
    line-height:auto;
}
#cvfc * {color:black !important;font-size:16px; box-sizing:border-box;margin:0}
#cvfc tr {padding-left:4ch; display:flex;justify-content:space-between; gap:0px;width:inherit}
#cvfc tr:nth-of-type(2n - 1) {background:white}
#cvfc td {background:transparent;position:relative;width:inherit;height:18px !important; white-space:nowrap}
#cvfc td:last-of-type{border-bottom:1px solid transparent; padding-left:10px}
#cvfc textarea{padding:0;margin:0;resize:none; width:40ch;white-space:nowrap;
border:none;background:none}
#cvfc table {display:none; background:none}
#cvfc:hover table {display:inherit}
#cvfc tr:hover td:last-of-type {background:lightgray;border-bottom:1px solid black}
#cvfc:hover {border-color:black; resize:horizontal;}
`;

const shovel_stylesheet = document.createElement("style");
shovel_stylesheet.innerHTML = shovel_style;
document.head.appendChild(shovel_stylesheet);

function filterEntriesWithCSSVariables(obj) {
    return Object.entries(obj).filter(([key, value]) => {
        return typeof value === "string" && value.startsWith("--");
    });
}
const style = getComputedStyle(document.documentElement);
const cssVars = filterEntriesWithCSSVariables(style).map((v) => v[1]); // A list of all :root var names
const processedCSSVars = {}; // An object of all :root var names paired with their current value

const container = document.createElement("section");
container.innerHTML = "<p></p>";
container.setAttribute("id", "cvfc");
document.body.appendChild(container);
const table = document.createElement("table");
container.appendChild(table);

cssVars.reverse().forEach((cssVar) => {
    let val = style.getPropertyValue(cssVar);
    processedCSSVars[cssVar] = val;
});

loadVarsFromServer();

// This function loads any pre-existing styles from the server and overwrites any localy stores custom styles!
function loadVarsFromServer() {
    // The domain name is sent to the server to identify the website!
    fetch(server_url + "?domain=" + window.location.hostname)
        .then(function (response) {
            return response.json();
        })
        .then(function (jsonResponse) {
            for (var processedCSSVar in processedCSSVars) {
                // Establish keys and values locally
                let key = processedCSSVar;
                let val = processedCSSVars[processedCSSVar];
                let serverStyles = jsonResponse.styles;

                // Update local styles with server versions
                if (serverStyles[key] != undefined) {
                    val = serverStyles[key];
                    processedCSSVars[key] = val;
                    document.body.style.setProperty(processedCSSVar, val);
                }
            }

            console.log("Shovel: Server success moving to render pass");
            renderPass();
        })
        .catch((error) => {
            // Server failed, run locally
            console.log("Shovel: Running in local mode - server error: " + error);
            renderPass();
        });
}

// This function handles updating the page with the new styles and generating the CSS editor
function renderPass() {
    cssVars.forEach((cssVar) => {
        // Update the page with any custom style vars!
        let val = processedCSSVars[cssVar];
        document.body.style.setProperty(cssVar, localStorage.getItem(cssVar) || val);

        // Generate the UI
        const tr = document.createElement("tr");
        const key = document.createElement("td");
        const value = document.createElement("td");

        const value_textArea = document.createElement("textarea");
        value_textArea.setAttribute("rows", 1);
        value_textArea.addEventListener("input", function () {
            // Update local storage with the new style
            localStorage.setItem(cssVar, this.value);
            document.body.style.setProperty(cssVar, this.value);
            processedCSSVars[cssVar] = this.value;

            // Update the server with the new style!
            fetch(server_url + "?domain=" + window.location.hostname + "&styleName=" + cssVar + "&styleValue=" + processedCSSVars[cssVar])
                .then(function (response) {
                    return response.json();
                })
                .then(function (jsonResponse) {
                    // The server will return a json of all styles, but we don't use it atm!
                    console.log("Style Updated on server!");
                });
        });

        value_textArea.innerHTML = localStorage.getItem(cssVar) || val;
        value.appendChild(value_textArea);

        key.innerHTML = cssVar;

        tr.appendChild(key);
        tr.appendChild(value);

        table.appendChild(tr);
    });
}

// This is a small server that allows you to sync changes for each domain across all people using the extension
// Also hello, I hope your havn a nice day! ~daniel

let sh = {};
sh.firstRun = true;

// A small database to save the edits across server restarts
const JSONdb = require("simple-json-db");
const minidb = new JSONdb("../mini-db.json", { asyncWrite: true });

sh.storage = {};

sh.template = `
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Declarations ~ Shovel</title>
        <style>
        body {
            background-color: white;
            margin: 50px;
            background-image: linear-gradient(45deg, #ffffff 47.92%, #ccffff 47.92%, #ccffff 50%, #ffffff 50%, #ffffff 97.92%, #ccffff 97.92%, #ccffff 100%);
            background-size: 33.94px 33.94px;
        }
        header {
            background-color: rgba(0,10,10,0.75);
            border-radius: 1em;
            color: white;
            padding: 10px 20px;
        }
        header h1 {
            color: #0fff;
        }
        footer {
            font-size: 0.8em;
            padding-top: 30px;
        }

        a, a:visited {
            color: currentcolor;
        }
        a:hover {
            text-decoration: none;
        }
        td {
            padding-left: 10px;
        }
        td:nth-of-type(even) {
           background-color: rgba(200,255,255,0.25);
        }
        </style>
    </head>
    <body>
        <header>
            <h1>Declarations ~ Shovel Extention</h1>
            <p>An art Firefox extension that exposes :root CSS values of a webpage and allows you and other visitors to modify them together. Modifications are synced via the Declarations web server and visible to everyone using the extension.</p>
            <p>Download the extention <a href="https://tell.declarations.style/static/files/declarations-shovel.xpi" target="_blank">here!</a> (Open your Addons Settings, click the cog and choose "Install from File")</p>
        </header>

        <section>
            <h2>Current Community Modifications:</h2>
            {STYLETABLE}
        </section>
    </body>
</html>
`;

// Main function that receives the request and sends the HTTP response.
async function go(req, res) {
    // Setup the cache from the servers database
    if (sh.firstRun) {
        sh.firstRun = false;
        if (minidb.has("shovel")) {
            sh.storage = JSON.parse(minidb.get("shovel"));
        }
    }

    // Gather all GET params
    let domain = req.query.domain;
    let styleName = req.query.styleName;
    let styleValue = req.query.styleValue;

    if (domain == undefined) {
        const sortedDomains = Object.fromEntries(Object.entries(sh.storage).sort(([, a], [, b]) => Object.keys(b).length - Object.keys(a).length));
        let styleTable = "";
        styleTable += "<section>";
        for (let domain in sortedDomains) {
            if (isEmpty(sh.storage[domain])) {
                continue;
            }
            styleTable += "<h3>✎ <a href='http://" + domain + "'>" + domain + "</a></h3>";
            styleTable += "<table>";
            styleTable += "<tr><th>Variable:</th><th>Value:</th></tr>";

            for (let style in sh.storage[domain]) {
                styleTable += "<tr>";
                styleTable += "<td>" + style + "</td>";
                styleTable += "<td>" + sh.storage[domain][style] + "</td>";
                styleTable += "</tr>";
            }

            styleTable += "</table>";
        }
        styleTable += "</section>";

        res.send(sh.template.replace("{STYLETABLE}", styleTable));
        return;
    }

    // Check if a style update was included in the request
    if (styleName != undefined && styleValue != undefined) {
        setStyles(domain, styleName, styleValue);
    }

    // Generate the final response JSON
    let response = {};
    response.styles = getStyles(domain);

    res.send(response);
}

// Get styles from the cache
function getStyles(domain) {
    if (sh.storage[domain] == undefined) {
        sh.storage[domain] = {};
        return {};
    }
    return sh.storage[domain];
}

// Put style in the cache - one style per request!
function setStyles(domain, styleName, styleValue) {
    if (sh.storage[domain] == undefined) {
        sh.storage[domain] = {};
    }
    sh.storage[domain][styleName] = styleValue;
    // Update the entire database for the domain - kinda clunky, but whatever! Someone can improve this if they want
    minidb.set("shovel", JSON.stringify(sh.storage));
}

// Checks if an object has any data in it
function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

// Necessary for Node.js module functionality - this is the exposed interface of the module!
module.exports = {
    go: function (req, res) {
        return go(req, res);
    },
};

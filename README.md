# declarative shovel

made by: doriane, martin, daniel
in: 1h (20min + install time)
during: declarations worksession between the 1st and the 5th of April
based on: https://git.vvvvvvaria.org/clemtre/cssVarFilledCookies.js
licence: CC4r License

---

A small extension that exposes editable CSS:root variables of websites. This allows people to make local and persistent edits to those variables, modifying the style of the website and creating an alternative look and feel.

we called it shovel because it removes a bit of dirt to show the `:root` (if the webpage have one)

```
          ___
         |_ _|
          | |
          | |
          | |
         _|_|_
        |  |  |
        \     /
         \___/
                           _____
                          /     \
  css variables         \/_      \     editable html table
                               ____________________________________
:root {                       |                                    |
  --underline-style: wavy;    |--underline-style : wavy            |
  --title-width: 10ch;        |--title-width     : 10ch            |
  --src: url('image.png');    |--src             : url('image.png')|
  --gradient-start: olive;    |--gradient-start  : olive           |
  --gradient-end: brown;      |--gradient-end    : brown           |
}                             |____________________________________|
                                __ 
                         \       /\
                          \_____/
```

the modifications are domain filtered because of how local storage works.
it's consistent between webpage of a same domain.

## collective

Synching styles to an online webserver, which would make changes persistant across every people using of the extention. 

This become a way to collectivize user-customizations.

To be surprised by other changes "oh someone else modified my root" as well as "cool someone already changed the font-size there it was shitty".

But also this is a shy shovel, because it only get to the `:root`, so everyone of let's say a specific community can decide what they expose or not to the shovel, like it remove some dirt but it don't go super far into the design.


## :root

about the root element: https://developer.mozilla.org/en-US/docs/Web/CSS/:root

> In HTML, :root represents the `<html>` element and is identical to the selector html, except that its specificity is higher.

## whishlist

* putting the fixed pop-up menu as a real pop-up extension menu
* Synching styles to an online webserver, which would make chnages persistant across every people using of the extention. 